
declare interface INavList {
  value: number
  label?: string
}

declare interface ITopList extends INavList {
  title: string
  class: string
}

declare interface ICodeMessage {
  [propName: number]: string
}
interface PageRequestParam {
  page: number;
  limit: number;
}
interface PageResult<T> {
  rows: T[];
  page?: {
    limit: number;
    page: number;
    total: number;
  }
}
declare namespace Article {
  interface ArticleListParam extends PageRequestParam {
    version: number;
    navId: number;
    title?: string;
  }
  interface Detail {
    appId: number;
    author: string;
    backend:any[];
    categoryId: number;
    cityId: number;
    commentCount: number;
    content: string;
    coverUrl:string;
    createTime:string;
    gitee:string;
    github:string;
    homepage:string;
    id: number;
    isTop: number;
    keywords:string;
    likeCount: number;
    md: string;
    navId: number;
    order: number;
    status: number;
    summary:string;
    tags:string[];
    title:string;
    updateTime:string;
    userId: number;
    version: string;
    viewsCount: number;
  }
  interface TagItem {
    appId: number;
    cityId: number;
    createTime: string;
    id: number;
    tagName: string;
    tagUrl: string;
    thumbnail: string;
    type: number;
    updateTime: string
  }
}